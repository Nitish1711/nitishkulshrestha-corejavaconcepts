package com.customHashmap;

public class CustomHashMap<K,V> {
  private Entry<K, V> tableArray[];
  private int capacity=4;
  
  public CustomHashMap() {
	// TODO Auto-generated constructor stub
	  tableArray=new Entry[capacity];
}
  
  public void put(K newKey,V value){
	  int hash=hash(newKey);
	  if(newKey==null){
		  return;
	  }
	Entry<K, V> newentry=new Entry<K,V>(newKey, value, null);
	if(tableArray[hash]==null){
		tableArray[hash]=newentry;
	}else{
		Entry<K, V> currentEntry=tableArray[hash];
		Entry<K, V> previousEntry=null;
		while(currentEntry!=null){
			if(currentEntry.getKey().equals(newKey)){
			if(previousEntry==null){
			//currentEntry.setValue(value);
				newentry.setNext(currentEntry.getNext());
				tableArray[hash]=newentry;
				return;
			}else{
				newentry.setNext(currentEntry.getNext());
			   previousEntry.setNext(newentry);
			   return;
			 }
			} 
			
			
			previousEntry=currentEntry;
			currentEntry=currentEntry.getNext();
		}
		previousEntry.setNext(newentry);
	}
	
  }
  
  public V get(K key){
	  int hash=hash(key);
	  Entry<K, V> temp=tableArray[hash];
	  while(temp!=null){
		  if(temp.getKey().equals(key))
			  return temp.getValue();
		  temp=temp.next;
	  }
	  return null;
  }
  
  private int hash(K key){
      return Math.abs(key.hashCode()) % capacity;
  }
  
	
}
