package com.customLinkedList;

public class Node {

	Object data;
	Node next;
	
	public Node(Object dataValue) {
		next = null;
		data = dataValue;
	}
	
	//these methods should be self-explanatory
	public Object getData() {
		return data;
	}

	@SuppressWarnings("unused")
	public void setData(Object dataValue) {
		data = dataValue;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node nextValue) {
		next = nextValue;
	}

	
}
