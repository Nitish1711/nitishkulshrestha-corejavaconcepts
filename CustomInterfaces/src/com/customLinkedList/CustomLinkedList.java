package com.customLinkedList;

public class CustomLinkedList {

	 Node head;
	 public static int counter;
	 
	 public void add(Object data){
		 if(head==null){
			 head=new Node(data);
		 }
		 Node currentNode=head;
		 Node tempNode=new Node(data);
		 
		 if(currentNode!=null){
			 while(currentNode.getNext()!=null){
				 currentNode=currentNode.getNext();
			 }
			 currentNode.setNext(tempNode);
		 }
	}
	 
	 //adding object on specified position 
	 public void add(Object data,int index){
		 if(head==null){
			 head=new Node(data);
		 }
		 Node currentNode=head;
		 Node tempNode=new Node(data);
		 
		 if(currentNode!=null){
			  for(int i=0;i<index;i++){
				  currentNode=currentNode.getNext();
			  }
			  tempNode.setNext(currentNode.getNext());
			  currentNode.setNext(tempNode);
		 }
	}
	 
	 public void remove(int  index){
		  
			 //head=new Node(data);
		  
		 Node currentNode=head;
		// Node tempNode=new Node(data);
		 
		 for(int i=0;i<index;i++){
			 currentNode=currentNode.getNext();
		 }
		 currentNode.setNext(currentNode.getNext().getNext());
		 
		 
	}
	 
}
