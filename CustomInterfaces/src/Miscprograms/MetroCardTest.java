/**
 * 
 */
package Miscprograms;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Nitish
 *
 */
public class MetroCardTest {

	private String Cardnumber;
	private double cardbalance;
	private String entryStation;
	private String exitStation;
	private String taking;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		entryStation="A2";
		exitStation="A4";
		taking="Exit";
		cardbalance=100;
		Cardnumber="33333";
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	 
	
	@Test
 	public void testAdd() {
		double totalfare = 21;
		MetroCard m=new MetroCard();
		double sum = m.processCard(Cardnumber, totalfare, entryStation, exitStation, taking);
		assertEquals(sum, totalfare,21);
		}


}
