package Miscprograms;

public class Staircase {

	public Staircase() {
		// TODO Auto-generated constructor stub
	}
	
	
	static void staircase(int n){
		
		 for (int i = 0; i < n; i++) {
	            for (int j =n-1; j >= 0; j--) {
	                if (j <= i) {
	                    System.out.print(" $");
	                } else {
	                    System.out.print(" ");
	                }
	            }
	            System.out.println();
		 }
	 

	}
	
	public static void main(String[] args) {
		staircase(6);
		
	}

}
