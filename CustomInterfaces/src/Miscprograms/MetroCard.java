/*
 * 
 * 	Our Company has been asked to implement Metro Smart Card System (MSCS) for Delhi City. For this application assume there is a 
 * single metro line covering 10 stations linearly.

	The stations name are A1,A2,A3,A4,A5,A6,A7,...A10. The travel can be in any direction from any station except A1 and A10.

	Travelers have smart card that behave just like any other regular debit card that has an initial balance when purchased.
Travelers swipe in when they enter a metro station and swipe out when they exit. Card balance is automatically updated at swipe out.

	Objective :

	Objective of this exercise is to create an automated system that has following functionality.

	Card should have a minimum balance of Rs. 50 at swipe-in. At swipe-out, system should calculate the fare based on below strategies. 
	The fare must be deducted from the card. Card should have the sufficient balance otherwise user should not be able to exit.

	Weekday - Rs 7 * (Number of stations travelled)
	Weekend - Rs 5.5 * (Number of station traveled if it is Saturday or Sunday )
	(* there could be more such fare strategies in future).

	Additionally system needs to have functionality to generate some statistics/report defined below. So system needs to provide following API.


	API to get total foot-fall(swipe-in + swipe-out) for a given station.
	API to generate a "per-card report" on demand. It should print the following information on console.
	      Card<number> used to travel from <source_station> to station <destination_station>. Fare is Rs<x> and balance on card is Rs<x>.
 */

package Miscprograms;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class MetroCard {
	
	public String stations []={"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10"};
	
	Calendar calendar = Calendar.getInstance();
	Date date = calendar.getTime();
	String currentday=new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

public static void main(String[] args) {
	String Cardnumber,entryStation,exitStation,taking;
	double cardbalance;
    Scanner in = new Scanner(System.in);

    System.out.println("Enter card number");
    Cardnumber = in.nextLine();
    
    Scanner in1 = new Scanner(System.in);

    System.out.println("Enter card balance");
    cardbalance = Double.valueOf(in1.nextLine());
    
    @SuppressWarnings("resource")
	Scanner in2 = new Scanner(System.in);

    System.out.println("Enter entry station");
    entryStation = in2.nextLine();
    
    Scanner in3 = new Scanner(System.in);

    System.out.println("Enter Exit station");
    exitStation = in3.nextLine();
    
    Scanner in4 = new Scanner(System.in);

    System.out.println("Enter taking");
    taking = in4.nextLine();
    
     MetroCard m=new MetroCard();
     m.processCard(Cardnumber, cardbalance, entryStation, exitStation, taking);
	
	 
	
 }

 
boolean swipeIn(double cardbalance){
	
	if(cardbalance<50){
		System.out.println("Please refill your card.cannot swipeIn!");
		return false;
	}else
		return true;
}

double swipeout(double cardbalance,int totalStations){
	System.out.println("Current day "+currentday);
	double fare=0;
	if(currentday.equalsIgnoreCase("Saturday") || currentday.equalsIgnoreCase("Sunday")){
		fare=5.5*totalStations;
	}else{
		fare=7*totalStations;
	}
	
	if(fare>cardbalance){
		System.out.println("Not enough balance to swipe out");
		return 0;
	}
	
	return fare;
}

public double processCard(String Cardnumber,double cardbalance,String entryStation,String exitStation,String taking){
	boolean swipeIn=false;
	boolean swipeout=false;
	double fare=0;
	
   if(taking.equalsIgnoreCase("Entry")){
	   swipeIn=true;
	   
   }else if(taking.equalsIgnoreCase("Exit")){
	   swipeout=true;
   }	
	String entry = null,exit=null;
	
	if(entryStation.equals("A1") || entryStation.equals("A10")){
		System.out.println("Entry restricted from these stations");
		return 0;
	}
	
	boolean movementFromRight=false;
	boolean movementFromleft=false;
	
	for(int i=0;i<stations.length;i++){
		if(stations[i].equalsIgnoreCase(entryStation)){
			System.out.println("left to right moving direction ");
			movementFromleft=true;
			break;
			 
		}else if(stations[i].equalsIgnoreCase(exitStation)){
			System.out.println("right to left moving direction ");
			movementFromRight=true;
			 break;
		}
	}
	
	int counter=0;
	boolean leftCountflag=false;
	boolean rightCountflag=false;
	
	
	
	if(swipeIn){
		swipeIn(cardbalance);
	}else if(swipeout){
		if(movementFromleft){
			for(int j=0;j<stations.length;j++){
				if(entryStation.equals(stations[j])){
					 leftCountflag=true;
					 
				}
				if(leftCountflag){
					counter++;
				}
				
				if(stations[j].equals(exitStation)){
					break;
				}
				
				
			}
		}else if(movementFromRight){
			
			for(int j=0;j<stations.length;j++){
				if(exitStation.equals(stations[j])){
					
					 rightCountflag=true;
					 
				}
				if(rightCountflag){
					counter++;
				}
				
				if(stations[j].equals(entryStation)){
					break;
				}
				
				
			}
			
		}
		
		 fare=swipeout(cardbalance,counter);
		System.out.println("TOtal fare "+fare);
		System.out.println("Remaining balance "+(cardbalance-fare));
		
	}
	
	
	return fare;
	
	
	
	
}



}
