package com.nitish;
 

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletDemo1 extends HttpServlet{
	
	public void init(ServletConfig config)
	{
	 System.out.println("Serveltconfig object is "+config.getInitParameter("test-param"));
	}
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException{
		ServletContext context=request.getSession().getServletContext();
		System.out.println("get context "+context.getInitParameter("globalVariable"));
		PrintWriter out = response.getWriter();
		String color= request.getParameter("color"); 
		String pwd= request.getParameter("pwd");   
		 
		//	request.getRequestDispatcher("view.jsp").forward(request,response);
			 response.sendRedirect("view.jsp");
		 

        //RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("../../WEB-INF/view.jsp");
        


		out.println("<html>");
		out.println("<body>");
		out.println("<h1> Username is "+color+"</h1>");
		out.println("<h1> Password is "+pwd+"</h1>");
		out.println("</body>");
		out.println("</html>");	
		
		
	    
	   

	}
}
