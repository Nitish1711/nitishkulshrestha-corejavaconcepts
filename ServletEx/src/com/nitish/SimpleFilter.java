package com.nitish;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class SimpleFilter implements Filter {
	 public void init(FilterConfig filterConfig) throws ServletException {
		  String testParam = filterConfig.getInitParameter("test-param");
		  System.out.println("filter param ");
	         
	        //Print the init parameter
	        System.out.println("Test Param: " + testParam);
	    }

	 public void doFilter(ServletRequest request, ServletResponse response,
             FilterChain filterChain)
throws IOException, ServletException {

String myParam = request.getParameter("pwd");
System.out.println("param "+myParam);
if(!"blockTheRequest".equals(myParam)){
filterChain.doFilter(request, response);
}
}


	    public void destroy() {
	    }

}
